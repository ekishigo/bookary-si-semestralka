package dao;

import com.google.common.collect.Lists;
import models.Author;
import models.Contract;
import models.Publisher;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class ContractDAOImplTest extends DaoTest {
    @Test
    public void test_saveContract() throws Exception {

        Publisher publisher = createPublisher();
        Author author = createAuthor();
        entityManager.persist(publisher);
        entityManager.persist(author);

        Contract contract = createContract(publisher, author);
        Contract saved = contractDAO.save(contract);
        Long idContract = saved.getId();
        assertTrue(idContract != null);
        Contract fetchedByEMfind = entityManager.find(Contract.class, idContract);
        assertEquals(saved, fetchedByEMfind);
        tearDown();
    }

    @Test
    public void test_deleteContract() throws Exception {
        Publisher publisher = createPublisher();
        Author author = createAuthor();
        entityManager.persist(publisher);
        entityManager.persist(author);

        Contract contract = createContract(publisher, author);
        entityManager.persist(contract);
        assertNotNull(contract.getId());
        contractDAO.delete(contract.getId());
        assertNull(entityManager.find(Contract.class, contract.getId()));
        tearDown();
    }

    @Test
    public void test_findContract() throws Exception {
        Publisher publisher = createPublisher();
        Author author = createAuthor();
        entityManager.persist(publisher);
        entityManager.persist(author);

        Contract contract = createContract(publisher, author);
        entityManager.persist(contract);
        assertNotNull(contract.getId());
        assertEquals(contract, contractDAO.find(contract.getId()));
        tearDown();
    }

    @Test
    public void test_updateContract() throws Exception {
        Publisher publisher = createPublisher();
        Author author = createAuthor();
        entityManager.persist(publisher);
        entityManager.persist(author);

        Contract contract = createContract(publisher, author);
        entityManager.persist(contract);
        Boolean singedByAuthor = contract.getSingedByAuthor();
        contract.setSingedByAuthor(true);
        assertTrue(!contract.getSingedByAuthor().equals(singedByAuthor));
        contractDAO.update(contract);
        assertEquals(contract, entityManager.find(Contract.class, contract.getId()));
        tearDown();
    }


    @Test
    public void test_listContracts() throws Exception {
        List<Contract> contracts = Lists.newArrayList();
        for (int i = 0; i < AUTHOR_NAMES.length; i++) {

            Publisher publisher = Publisher.create(PUBLISHER_NAMES[i]);
            Author author = new Author(AUTHOR_NAMES[i], AUTHOR_LAST_NAMES[i]);
            entityManager.persist(publisher);
            entityManager.persist(author);

            Contract contract = createContract(publisher, author);
            entityManager.persist(contract);
            assertNotNull(contract.getId());
            contracts.add(contract);
        }
        List<Contract> fetched = contractDAO.list();
        assertArrayEquals(contracts.toArray(), fetched.toArray());
        tearDown();
    }
}