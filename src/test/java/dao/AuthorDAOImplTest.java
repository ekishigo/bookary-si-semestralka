package dao;

import com.google.common.collect.Lists;
import models.Author;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class AuthorDAOImplTest extends DaoTest {

    @Test
    public void test_saveAuthor() throws Exception {
        Author author = createAuthor();
        Author saved = authorDAO.save(author);
        Long idAuthor = saved.getId();
        assertTrue(idAuthor != null);
        Author fetchedByEMfind = entityManager.find(Author.class, idAuthor);
        assertEquals(saved, fetchedByEMfind);

        tearDown();
    }

    @Test
    public void test_deleteAuthor() throws Exception {
        Author author = createAuthor();
        entityManager.persist(author);
        assertNotNull(author.getId());
        authorDAO.delete(author.getId());
        assertNull(entityManager.find(Author.class, author.getId()));

        tearDown();
    }

    @Test
    public void test_findAuthor() throws Exception {
        Author author = createAuthor();
        entityManager.persist(author);
        assertNotNull(author.getId());
        assertEquals(author, authorDAO.find(author.getId()));

        tearDown();
    }

    @Test
    public void test_updateAuthor() throws Exception {
        Author author = createAuthor();
        entityManager.persist(author);
        String email = author.getEmail();
        author.setEmail("someNewEmail@mail.com");
        assertTrue(!author.getEmail().equals(email));
        authorDAO.update(author);
        assertEquals(author, entityManager.find(Author.class, author.getId()));

        tearDown();
    }


    @Test
    public void test_listAuthors() throws Exception {
        List<Author> authors = Lists.newArrayList();
        for (int i = 0; i < AUTHOR_NAMES.length; i++) {
            Author a = new Author(AUTHOR_NAMES[i], AUTHOR_LAST_NAMES[i]);
            entityManager.persist(a);
            assertNotNull(a.getId());
            authors.add(a);
        }
        List<Author> fetched = authorDAO.list();
        assertArrayEquals(authors.toArray(), fetched.toArray());

        tearDown();
    }
}