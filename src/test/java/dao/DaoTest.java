package dao;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import exceptions.ContractException;
import models.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;
import util.Resource;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@RunWith(Arquillian.class)
@Transactional
public abstract class DaoTest {

    public static final String[] AUTHOR_NAMES = {
            "Fedor",
            "Karel",
            "Isaak"
    };
    public static final String[] AUTHOR_LAST_NAMES = {
            "Dostojevsky",
            "Capek",
            "Azimov"
    };
    public static final String[] PUBLISHER_NAMES = {
            "Academia",
            "Karolinum",
            "Librex"
    };
    public static final String[] BOOK_TITLES = {
            "Idiot",
            "R.U.R.",
            "Foundation"
    };
    public static final String[] BOOK_ISBNS = {
            "1",
            "2",
            "3"
    };
    public static final String[] LIBRARY_NAME = {
            "Knihovna CVUT",
            "Knihovna Clementium",
            "Knihovna Karlovy Univerzity"
    };
    public static final String[] LIBRARY_ADDRESSES = {
            "ulice 1",
            "ulice 2",
            "ulice 3"
    };
    @Inject
    protected EntityManager entityManager;
    @Inject
    protected AuthorDAO authorDAO;
    @Inject
    protected LibraryDAO libraryDAO;
    @Inject
    protected PublisherDAO publisherDAO;
    @Inject
    protected BookDAO bookDAO;
    @Inject
    protected ContractDAO contractDAO;

    @Deployment
    public static Archive<WebArchive> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage("models")
                .addPackage(DAO.class.getPackage())
                .addPackage(Lists.class.getPackage())
                .addPackage(Preconditions.class.getPackage())
                .addPackage(Ints.class.getPackage())
                .addPackage(ContractException.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    protected Author createAuthor() {
        return new Author(AUTHOR_NAMES[0], AUTHOR_LAST_NAMES[0]);
    }

    protected Contract createContract(Publisher publisher, Author author) {
        return Contract.create(publisher, author);
    }

    protected Publisher createPublisher() {
        return Publisher.create(PUBLISHER_NAMES[0]);
    }

    protected Book createBook(List<Author> authors, String bookIsbn, String bookTitle) {
        return Book.create(bookTitle, bookIsbn, authors);
    }

    protected Library createLibrary(String name, String address) {
        return Library.create(name, address);
    }

    public void tearDown() throws Exception {
        entityManager.createQuery("select e from Contract e").getResultList().forEach(o -> entityManager.remove(o));
        entityManager.createQuery("select e from Publisher e").getResultList().forEach(o -> entityManager.remove(o));
        entityManager.createQuery("select e from Author e").getResultList().forEach(o -> entityManager.remove(o));
        entityManager.createQuery("select e from Book e").getResultList().forEach(o -> entityManager.remove(o));
        entityManager.createQuery("select e from Library e").getResultList().forEach(o -> entityManager.remove(o));
    }
}
