package dao;

import com.google.common.collect.Lists;
import models.Publisher;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class PublisherDAOImplTest extends DaoTest{



    @Test
    public void test_savePublisher() throws Exception {
        Publisher publisher = createPublisher();
        Publisher saved = publisherDAO.save(publisher);
        Long idPublisher = saved.getId();
        assertTrue(idPublisher != null);
        Publisher fetchedByEMfind = entityManager.find(Publisher.class, idPublisher);
        assertEquals(saved, fetchedByEMfind);

        tearDown();
    }

    @Test
    public void test_deletePublisher() throws Exception {
        Publisher publisher = createPublisher();
        entityManager.persist(publisher);
        assertNotNull(publisher.getId());
        publisherDAO.delete(publisher.getId());
        assertNull(entityManager.find(Publisher.class, publisher.getId()));

        tearDown();
    }

    @Test
    public void test_findPublisher() throws Exception {
        Publisher publisher = createPublisher();
        entityManager.persist(publisher);
        assertNotNull(publisher.getId());
        assertEquals(publisher, publisherDAO.find(publisher.getId()));
        tearDown();
    }

    @Test
    public void test_updatePublisher() throws Exception {
        Publisher publisher = createPublisher();
        entityManager.persist(publisher);
        String address = publisher.getName();
        publisher.setName("someNewName");
        assertTrue(!publisher.getName().equals(address));
        publisherDAO.update(publisher);
        assertEquals(publisher, entityManager.find(Publisher.class, publisher.getId()));
        tearDown();
    }


    @Test
    public void test_listPublishers() throws Exception {
        List<Publisher> publishers = Lists.newArrayList();
        for (int i = 0; i < AUTHOR_NAMES.length; i++) {
            Publisher publisher = Publisher.create(PUBLISHER_NAMES[i]);
            entityManager.persist(publisher);
            assertNotNull(publisher.getId());
            publishers.add(publisher);
        }
        List<Publisher> fetched = publisherDAO.list();
        assertArrayEquals(publishers.toArray(), fetched.toArray());
        tearDown();
    }


}