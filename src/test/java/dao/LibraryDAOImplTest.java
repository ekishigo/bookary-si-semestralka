package dao;

import com.google.common.collect.Lists;
import models.Library;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class LibraryDAOImplTest extends DaoTest{


    @Test
    public void test_saveLibrary() throws Exception {
        Library library = createLibrary(LIBRARY_NAME[0], LIBRARY_ADDRESSES[0]);
        Library saved = libraryDAO.save(library);
        Long idLibrary = saved.getId();
        assertTrue(idLibrary != null);
        Library fetchedByEMfind = entityManager.find(Library.class, idLibrary);
        assertEquals(saved, fetchedByEMfind);

        tearDown();
    }

    @Test
    public void test_deleteLibrary() throws Exception {
        Library library = createLibrary(LIBRARY_NAME[0], LIBRARY_ADDRESSES[0]);
        entityManager.persist(library);
        assertNotNull(library.getId());
        libraryDAO.delete(library.getId());
        assertNull(entityManager.find(Library.class, library.getId()));

        tearDown();
    }

    @Test
    public void test_findLibrary() throws Exception {
        Library library = createLibrary(LIBRARY_NAME[0], LIBRARY_ADDRESSES[0]);
        entityManager.persist(library);
        assertNotNull(library.getId());
        assertEquals(library, libraryDAO.find(library.getId()));
        tearDown();
    }

    @Test
    public void test_updateLibrary() throws Exception {
        Library library = createLibrary(LIBRARY_NAME[0], LIBRARY_ADDRESSES[0]);
        entityManager.persist(library);
        String address = library.getAddress();
        library.setAddress("someNewAddress");
        assertTrue(!library.getAddress().equals(address));
        libraryDAO.update(library);
        assertEquals(library, entityManager.find(Library.class, library.getId()));
        tearDown();
    }


    @Test
    public void test_listLibrarys() throws Exception {
        List<Library> librarys = Lists.newArrayList();
        for (int i = 0; i < AUTHOR_NAMES.length; i++) {
            Library library = createLibrary(LIBRARY_NAME[0], LIBRARY_ADDRESSES[0]);
            entityManager.persist(library);
            assertNotNull(library.getId());
            librarys.add(library);
        }
        List<Library> fetched = libraryDAO.list();
        assertArrayEquals(librarys.toArray(), fetched.toArray());
        tearDown();
    }

}