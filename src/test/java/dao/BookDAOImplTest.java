package dao;

import com.google.common.collect.Lists;
import models.Author;
import models.Book;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class BookDAOImplTest extends DaoTest{

    @Test
    public void test_saveBook() throws Exception {
        Author author = createAuthor();
        entityManager.persist(author);


        Book book = createBook(Lists.newArrayList(author), BOOK_ISBNS[0], BOOK_TITLES[0]);
        Book saved = bookDAO.save(book);
        Long idBook = saved.getId();
        assertTrue(idBook != null);
        Book fetchedByEMfind = entityManager.find(Book.class, idBook);
        assertEquals(saved, fetchedByEMfind);

        tearDown();
    }

    @Test
    public void test_deleteBook() throws Exception {
        Author author = createAuthor();
        entityManager.persist(author);


        Book book = createBook(Lists.newArrayList(author), BOOK_ISBNS[0], BOOK_TITLES[0]);
        entityManager.persist(book);
        assertNotNull(book.getId());
        bookDAO.delete(book.getId());
        assertNull(entityManager.find(Book.class, book.getId()));

        tearDown();
    }

    @Test
    public void test_findBook() throws Exception {
        Author author = createAuthor();
        entityManager.persist(author);


        Book book = createBook(Lists.newArrayList(author), BOOK_ISBNS[0], BOOK_TITLES[0]);
        entityManager.persist(book);
        assertNotNull(book.getId());
        assertEquals(book, bookDAO.find(book.getId()));

        tearDown();
    }

    @Test
    public void test_updateBook() throws Exception {
        Author author = createAuthor();
        entityManager.persist(author);


        Book book = createBook(Lists.newArrayList(author), BOOK_ISBNS[0], BOOK_TITLES[0]);
        entityManager.persist(book);
        String title = book.getTitle();
        book.setTitle("someNewTitle");
        assertTrue(!book.getTitle().equals(title));
        bookDAO.update(book);
        assertEquals(book, entityManager.find(Book.class, book.getId()));

        tearDown();
    }


    @Test
    public void test_listBooks() throws Exception {
        List<Book> books = Lists.newArrayList();
        for (int i = 0; i < AUTHOR_NAMES.length; i++) {

            Author author = new Author(AUTHOR_NAMES[i], AUTHOR_LAST_NAMES[i]);
            entityManager.persist(author);

            Book book = createBook(Lists.newArrayList(author), BOOK_ISBNS[i], BOOK_TITLES[i]);

            entityManager.persist(book);
            assertNotNull(book.getId());
            books.add(book);
        }
        List<Book> fetched = bookDAO.list();
        assertArrayEquals(books.toArray(), fetched.toArray());

        tearDown();
    }

}