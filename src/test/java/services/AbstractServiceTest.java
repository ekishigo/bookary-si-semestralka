package services;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import dao.*;
import exceptions.ContractException;
import models.*;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;
import util.Resource;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@RunWith(Arquillian.class)
@Transactional
public abstract class AbstractServiceTest {
    @Inject
    protected ContractService contractService;
    @Inject
    protected PublisherService publisherService;
    @Inject
    protected LibraryService libraryService;
    @Inject
    protected PublisherDAO publisherDAO;
    @Inject
    protected AuthorDAO authorDAO;
    @Inject
    protected BookDAO bookDAO;
    @Inject
    protected LibraryDAO libraryDAO;
    @Inject
    protected ContractDAO contractDAO;
    protected Publisher publisher;
    protected Book book;
    protected List<Author> authors;
    protected List<LibraryBook> libraryBooks;
    protected Library library;

    @Deployment
    public static Archive<WebArchive> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(ContractService.class.getPackage())
                .addPackage(Contract.class.getPackage())
                .addPackage(ContractDAO.class.getPackage())
                .addPackage(Lists.class.getPackage())
                .addPackage(Ints.class.getPackage())
                .addPackage(Preconditions.class.getPackage())
                .addPackage(ContractException.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    protected void dropData() {
        authors.forEach(author -> authorDAO.delete(author.getId()));
        libraryDAO.delete(library.getId());
        bookDAO.delete(book.getId());
        publisherDAO.delete(publisher.getId());
    }

    protected void initData() {
        initDataForException();

        List<Contract> contracts = Lists.newArrayList();
        libraryBooks = Lists.newArrayList();
        for (Author author : authors) {
            Contract contract = contractService.publisherOfferContractToAuthor(publisher.getId(), author.getId());
            contracts.add(contract);
            contractService.authorConcludeContract(contract.getId());
        }

        library = Library.create("some lib", "on some street");
        libraryDAO.save(library);
    }

    protected void initDataForException() {
        publisher = Publisher.create("some publisher");
        publisherDAO.save(publisher);

        authors = Lists.newArrayList(Author.create("Fedor", "Dostojevsky"), Author.create("Karel", "Capek"));
        authors.forEach(author -> authorDAO.save(author));

        book = Book.create("isbn", "Foundation", authors);
        bookDAO.save(book);

        assert publisher.getId() != null;
        for (Author author : authors) {
            assert author.getId() != null;
        }
        assert book.getId() != null;
    }
}
