package services;

import models.Author;
import models.Contract;
import models.ContractState;
import models.Publisher;
import org.junit.Test;

import static dao.DaoTest.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class ContractServiceImplTest extends AbstractServiceTest {

    private Publisher publisher;
    private Author author;
    private Contract contract;

    private void initAuthorAndPublisher() throws Exception {
        publisher = Publisher.create(PUBLISHER_NAMES[0]);
        publisherDAO.save(publisher);
        author = Author.create(AUTHOR_NAMES[0], AUTHOR_LAST_NAMES[0]);
        authorDAO.save(author);
    }

    private void dropAuthorAndPublisher() throws Exception {
        authorDAO.delete(author.getId());
        author = null;
        publisherDAO.delete(publisher.getId());
        publisherDAO = null;
    }

    private void initContract() {
        assert author != null;
        assert publisher != null;
        contract = contractService.publisherOfferContractToAuthor(publisher.getId(), author.getId());
    }


    @Test
    public void test_publisherOfferContractToAuthor() throws Exception {
        initAuthorAndPublisher();

        Contract contract = contractService.publisherOfferContractToAuthor(publisher.getId(), author.getId());
        assertNotNull(contract.getId());

        assertThat(contract.getAuthor(), is(author));
        assertThat(contract.getPublisher(), is(publisher));

        assertTrue(contract.getSignedByPublisher());
        assertTrue(!contract.getSingedByAuthor());

        assertNotNull(contract.getInitDate());
        assertNull(contract.getConcludedDate());

        assertThat(contract.getState(), is(ContractState.INITIALIZED));

        dropAuthorAndPublisher();
    }


    @Test
    public void test_authorConcludeContract() throws Exception {
        initAuthorAndPublisher();
        initContract();

        Contract contract = contractService.authorConcludeContract(this.contract.getId());

        assertThat(contract.getState(), is(ContractState.CONCLUDED));
        assertNotNull(contract.getConcludedDate());

        dropAuthorAndPublisher();
    }

}