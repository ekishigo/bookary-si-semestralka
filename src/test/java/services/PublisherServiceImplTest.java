package services;

import exceptions.ContractException;
import models.Book;
import models.BookState;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class PublisherServiceImplTest extends AbstractServiceTest {

    @Test(expected = ContractException.class)
    public void test_publishBook_for_Authors_Without_Contract() throws Exception {
        initDataForException();
        Book publishBook = publisherService.publishBook(book.getId(), publisher.getId());
        dropData();
    }

    @Test
    public void test_publishBook_for_Authors() throws Exception {
        initData();

        Book publishBook = publisherService.publishBook(book.getId(), publisher.getId());

        assertThat(publishBook.getBookState(), is(BookState.PUBLISHED));
        assertNotNull(publishBook.getDateOfPublication());

        dropData();
    }


}