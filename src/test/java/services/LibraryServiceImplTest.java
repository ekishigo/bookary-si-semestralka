package services;

import exceptions.LibraryBookException;
import models.Author;
import models.Contract;
import models.LibraryBook;
import org.hamcrest.core.Is;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public class LibraryServiceImplTest extends AbstractServiceTest {
    @Test(expected = LibraryBookException.class)
    public void test_addCopyToLibrary_bookNotPublished() throws Exception {
        initData();
        assert !book.published();
        addToLib();
        dropData();
    }

    private void addToLib() {
        LibraryBook libraryBook = libraryService.addCopyToLibrary(book.getId(), library.getId());
        assertNotNull(libraryBook);
        boolean contains = library.getCollectedBooks().contains(libraryBook);
        assertTrue(contains);
        assertTrue(libraryBook.getCopyCounter() <= 5);
        dropData();
    }

    @Test(expected = LibraryBookException.class)
    public void test_addCopyToLibrary_moreThan5Copies() throws Exception {
        initData();
        prepContractPublishBook();
        for (int i = 0; i < 6; i++) {
            LibraryBook libraryBook = libraryService.addCopyToLibrary(book.getId(), library.getId());
            libraryBooks.add(libraryBook);
            assertNotNull(libraryBook);
        }
        dropData();
    }

    private void prepContractPublishBook() {
        for (Author author : authors) {
            Contract contract = contractService.publisherOfferContractToAuthor(publisher.getId(), author.getId());
            contractService.authorConcludeContract(contract.getId());
        }
        publisherService.publishBook(book.getId(), publisher.getId());
    }


    @Test
    public void test_addCopyToLibrary() throws Exception {
        initData();
        prepContractPublishBook();
        LibraryBook libraryBook1 = libraryService.addCopyToLibrary(book.getId(), library.getId());
        boolean contains = library.getCollectedBooks().contains(libraryBook1);
        assertTrue(contains);
        assertThat(libraryBook1.getBorrowedCounter(), Is.is(0));
        assertThat(libraryBook1.getCopyCounter(), Is.is(1));
        libraryBooks.add(libraryBook1);
        LibraryBook libraryBook2 = libraryService.addCopyToLibrary(book.getId(), library.getId());
        assertThat(libraryBook2.getBorrowedCounter(), Is.is(0));
        assertThat(libraryBook2.getCopyCounter(), Is.is(2));
        libraryBooks.add(libraryBook2);
        dropData();
    }

}