package models;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import exceptions.LibraryBookException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Entity
public class Library implements Serializable {

    private Long id;
    private String name;
    private String address;
    private List<LibraryBook> collectedBooks;

    public Library() {
    }

    private Library(String name, String address) {
        this.name = name;
        this.address = address;
        this.collectedBooks = Lists.newArrayList();
    }

    public static Library create(String name, String address) {
        return new Library(name, address);
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @SuppressWarnings("JpaDataSourceORMInspection")
    @OneToMany(cascade = CascadeType.MERGE, fetch = EAGER, orphanRemoval = true)
    @NotNull
    @JoinColumn(name = "librarybook_id")
    public List<LibraryBook> getCollectedBooks() {
        return collectedBooks;
    }

    public void setCollectedBooks(List<LibraryBook> collectedBooks) {
        this.collectedBooks = collectedBooks;
    }

    public LibraryBook addCopy(Book book) {
        if (!book.published()) {
            String errMsg = String.format("book %s is not published yet", book.toString());
            throw new LibraryBookException(errMsg);
        }
        LibraryBook libraryBook = findInLibrary(book);
        if (libraryBook != null && libraryBook.getCopyCounter() == 5) {
            String errMsg = String.format("library %s already owns five copies of book %s", this.toString(), book.toString());
            throw new LibraryBookException(errMsg);
        } else if (libraryBook != null) {
            libraryBook.incrementCopyCounter();
            return libraryBook;
        } else {
            libraryBook = new LibraryBook(book);
            this.getCollectedBooks().add(libraryBook);
            return libraryBook;
        }
    }

    private LibraryBook findInLibrary(Book book) {
        LibraryBook lb = new LibraryBook(book);
        collectedBooks.sort(LibraryBook::compareTo);
        int i = Ordering.from(LibraryBook::compareTo).binarySearch(collectedBooks, lb);
        if (i >= 0) {
            return collectedBooks.get(i);
        }
        return null;
    }

    @Override
    public String toString() {
        return name;
    }
}
