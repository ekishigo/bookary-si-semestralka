package models;

import com.google.common.collect.Lists;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Entity
public class Publisher implements Serializable {

    private Long id;
    private String name;
    private String contactAddress;
    private String contactEmail;
    private List<Contract> contractsWithAuthors;
    private List<Book> publishedBooks;

    public Publisher() {
    }

    private Publisher(String name) {
        this.name = name;
        contractsWithAuthors = Lists.newArrayList();
        publishedBooks = Lists.newArrayList();
    }

    public static Publisher create(String name) {
        return new Publisher(name);
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    @Email
    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @OneToMany(mappedBy = "publisher", orphanRemoval = true, fetch = FetchType.EAGER)
    @NotNull
    public List<Contract> getContractsWithAuthors() {
        return contractsWithAuthors;
    }

    public void setContractsWithAuthors(List<Contract> contractsWithAuthors) {
        this.contractsWithAuthors = contractsWithAuthors;
    }

    @NotNull
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "publisher")
    public List<Book> getPublishedBooks() {
        return publishedBooks;
    }

    public void setPublishedBooks(List<Book> publishedBooks) {
        this.publishedBooks = publishedBooks;
    }

    @Override
    public String toString() {
        return name;
    }

    public Contract makeContractFor(@NotNull Author author) {
        Contract contract = Contract.create(this, author);

        contractsWithAuthors.add(contract);

        return contract;
    }

    public void sendContractToAuthor(@NotNull Contract contract, @NotNull Author author) {
        author.addContract(contract);
    }
}
