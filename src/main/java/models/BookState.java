package models;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public enum BookState {
    WRITTEN, PUBLISHED
}
