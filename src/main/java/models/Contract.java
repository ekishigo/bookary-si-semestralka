package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Entity
public class Contract {

    private Long id;
    private Publisher publisher;
    private Boolean signedByPublisher;
    private Author author;
    private Boolean singedByAuthor;
    private ContractState state;
    private Date initDate;
    private Date concludedDate;

    public Contract() {
    }

    private Contract(Publisher publisher, Author author) {
        this.publisher = publisher;
        signedByPublisher = true;
        this.author = author;
        singedByAuthor = false;
        this.state = ContractState.INITIALIZED;
        this.initDate = new Date(System.currentTimeMillis());
    }

    public static Contract create(Publisher publisher, Author author) {
        return new Contract(publisher, author);
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher offerParty) {
        this.publisher = offerParty;
    }

    @ManyToOne
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author acceptanceParty) {
        this.author = acceptanceParty;
    }

    @NotNull
    public ContractState getState() {
        return state;
    }

    public void setState(ContractState state) {
        this.state = state;
    }

    @NotNull
    public Boolean getSignedByPublisher() {
        return signedByPublisher;
    }

    public void setSignedByPublisher(Boolean signedByPublisher) {
        this.signedByPublisher = signedByPublisher;
    }

    @NotNull
    public Boolean getSingedByAuthor() {
        return singedByAuthor;
    }

    public void setSingedByAuthor(Boolean singedByAuthor) {
        this.singedByAuthor = singedByAuthor;
    }

    @NotNull
    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getConcludedDate() {
        return concludedDate;
    }

    public void setConcludedDate(Date concludedDate) {
        this.concludedDate = concludedDate;
    }


    public void conclude() {
        singedByAuthor = true;
        concludedDate = new Date(System.currentTimeMillis());
        state = ContractState.CONCLUDED;
    }

    @Override
    public String toString() {
        return "Contract: " + publisher +
                " -> " + author;
    }
}
