package models;

import exceptions.ContractException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Entity
public class Book implements Serializable {

    private Long id;
    private String isbn;
    private String title;
    private Date dateOfPublication;
    private BookState bookState;
    private List<Author> authors;
    private Publisher publisher;

    public Book() {
    }

    private Book(String isbn, String bookTitle, List<Author> authors) {
        this.isbn = isbn;
        this.title = bookTitle;
        this.authors = authors;
        this.bookState = BookState.WRITTEN;
    }

    public static Book create(String isbn, String bookTitle, List<Author> authors) {
        return new Book(isbn, bookTitle, authors);
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @Column(unique = true)
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @NotNull
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateOfPublication() {
        return dateOfPublication;
    }

    public void setDateOfPublication(Date dateOfPublication) {
        this.dateOfPublication = dateOfPublication;
    }

    @NotNull
    public BookState getBookState() {
        return bookState;
    }

    public void setBookState(BookState bookState) {
        this.bookState = bookState;
    }

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "writtenBooks")
    @NotNull
    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @ManyToOne
    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public void publish(Publisher publisher) {
        if (eachAuthorHasConcludedContractWith(publisher)) {
            dateOfPublication = new Date(System.currentTimeMillis());
            bookState = BookState.PUBLISHED;
            this.publisher = publisher;
            return;
        }
        String errMsg = String.format("Someone of %s's authors has not concluded contract with publisher %s", this.toString(), publisher.toString());
        throw new ContractException(errMsg);
    }

    private boolean eachAuthorHasConcludedContractWith(Publisher publisher) {
        for (Author author : authors) {
            if (!author.hasConcludedContractWith(publisher)) {
                return false;
            }
        }
        return true;
    }

    public boolean published() {
        return bookState.equals(BookState.PUBLISHED);
    }

    @Override
    public String toString() {
        return title;
    }
}
