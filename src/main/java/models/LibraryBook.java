package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Entity
public class LibraryBook implements Serializable, Comparable<LibraryBook> {

    private Long id;
    private Book book;
    private Integer copyCounter;
    private Integer borrowedCounter;


    public LibraryBook() {
    }

    public LibraryBook(Book book) {
        this.book = book;
        this.copyCounter = 1;
        this.borrowedCounter = 0;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getCopyCounter() {
        return copyCounter;
    }

    public void setCopyCounter(Integer copyCounter) {
        this.copyCounter = copyCounter;
    }

    public Integer getBorrowedCounter() {
        return borrowedCounter;
    }

    public void setBorrowedCounter(Integer borrowedCounter) {
        this.borrowedCounter = borrowedCounter;
    }

    public void incrementCopyCounter() {
        copyCounter++;
    }

    @Override
    public int compareTo(LibraryBook o) {
        return this.getBook().getIsbn().compareTo(o.getBook().getIsbn());
    }
}
