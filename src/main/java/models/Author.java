package models;

import com.google.common.collect.Lists;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Entity
public class Author implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private List<Book> writtenBooks;
    private List<Contract> contractsWithPublishers;

    public Author() {
    }

    public Author(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.writtenBooks = Lists.newArrayList();
        this.contractsWithPublishers = Lists.newArrayList();
    }

    public static Author create(String firstName, String lastName) {
        return new Author(firstName, lastName);
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @SuppressWarnings("JpaDataSourceORMInspection")
    @NotNull
    @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(name = "author_book", joinColumns = @JoinColumn(name = "author_id"), inverseJoinColumns = @JoinColumn(name = "book_id"))
    public List<Book> getWrittenBooks() {
        return writtenBooks;
    }

    public void setWrittenBooks(List<Book> writtenBooks) {
        this.writtenBooks = writtenBooks;
    }

    @NotNull
    @OneToMany(mappedBy = "author", orphanRemoval = true, fetch = FetchType.EAGER)
    public List<Contract> getContractsWithPublishers() {
        return contractsWithPublishers;
    }

    public void setContractsWithPublishers(List<Contract> contractsWithPublishers) {
        this.contractsWithPublishers = contractsWithPublishers;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public void addContract(Contract contract) {
        this.contractsWithPublishers.add(contract);
    }

    public boolean hasConcludedContractWith(Publisher publisher) {
        Contract contract = findContractWith(publisher);
        return contract != null && ContractState.CONCLUDED.equals(contract.getState());
    }

    private Contract findContractWith(Publisher publisher) {
        Optional<Contract> contractOptional = contractsWithPublishers.stream().filter(contract -> contract.getPublisher().getId().equals(publisher.getId())).findFirst();
        return contractOptional.orElse(null);
    }
}
