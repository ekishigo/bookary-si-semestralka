package controllers;

import dao.AuthorDAO;
import dao.PublisherDAO;
import models.Author;
import models.Contract;
import models.Publisher;
import org.slf4j.Logger;
import services.ContractService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@SuppressWarnings("WeakerAccess")
@ManagedBean
@ViewScoped
public class ContractController {
    @Inject
    private ContractService contractService;
    @Inject
    private Logger logger;
    @Inject
    private PublisherDAO publisherDAO;
    @Inject
    private AuthorDAO authorDAO;
    @Inject
    private FacesContext facesContext;

    private Long selectedContractId;

    public List<Author> getAuthorList() {
        logger.info("Getting author list by DAO.");
        return authorDAO.list();
    }

    public List<Publisher> getPublisherList() {
        logger.info("Getting publisher list by DAO.");
        return publisherDAO.list();
    }

    public Long getSelectedContractId() {
        return selectedContractId;
    }

    public void setSelectedContractId(Long selectedContractId) {
        this.selectedContractId = selectedContractId;
    }

    public void cocnludeContract() {

        if (selectedContractId == null) {
            facesContext.addMessage("contractForm:confirmButton",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Select some contract please.", null)
            );
            return;
        }
        @SuppressWarnings("unused") Contract contract = contractService.authorConcludeContract(selectedContractId);

        facesContext.addMessage("contractForm:confirmButton",
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Contract was successfully concluded.", null));
        selectedContractId = null;
    }


}
