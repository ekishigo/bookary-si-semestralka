package controllers;

import dao.AuthorDAO;
import dao.LibraryDAO;
import dao.PublisherDAO;
import models.Author;
import models.Library;
import models.Publisher;
import org.slf4j.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@SuppressWarnings("WeakerAccess")
@ManagedBean
@ViewScoped
public class CatalogController {
    @Inject
    private Logger logger;
    @Inject
    private AuthorDAO authorDAO;
    @Inject
    private PublisherDAO publisherDAO;
    @Inject
    private LibraryDAO libraryDAO;

    public List<Author> getAuthors() {
        logger.info("Getting authors by DAO.");
        return authorDAO.list();
    }

    public List<Publisher> getPublishers() {
        logger.info("Getting publishers by DAO.");
        return publisherDAO.list();
    }

    public List<Library> getLibriries() {
        logger.info("Getting libriries by DAO.");
        return libraryDAO.list();
    }
}
