package controllers;

import dao.AuthorDAO;
import dao.BookDAO;
import dao.PublisherDAO;
import exceptions.ContractException;
import models.Author;
import models.Book;
import models.Contract;
import models.Publisher;
import org.slf4j.Logger;
import services.ContractService;
import services.PublisherService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@SuppressWarnings("WeakerAccess")
@ManagedBean
@ViewScoped
public class PublisherController {
    @Inject
    private Logger log;
    @Inject
    private PublisherDAO publisherDAO;
    @Inject
    private BookDAO bookDAO;
    @Inject
    private AuthorDAO authorDAO;
    @Inject
    private FacesContext facesContext;

    @Inject
    private PublisherService publisherService;
    @Inject
    private ContractService contractService;

    private Long publisherId;
    private Publisher publisher;

    private Long selectedAuthorId;
    private Long selectedBookId;


    public void init() {

        if (publisherId == null) {
            badRequestMessage();
            return;
        }
        log.info("initializing publisher contoller for publisher [id:{}]", publisherId);
        publisher = publisherDAO.find(publisherId);

        if (publisher == null) {
            String message = "Bad request. Unknown publisher.";
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
            return;
        }
        log.info("publisher [id:{}] was initialized", publisherId);
    }

    private void badRequestMessage() {
        String message = "Bad request. Please use a link from within the system.";
        facesContext.addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    public void offerContract() {
        log.info("offering contract to author");
        if (selectedAuthorId == null) {
            log.info("bad request!");
            String message = "Please select author.";
            facesContext.addMessage("offerContractForm:offerContractBtn",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
            return;
        }

        Contract contract = contractService.publisherOfferContractToAuthor(publisherId, selectedAuthorId);
        if (contract == null) {
            log.info("bad request!");
            badRequestMessage();
        } else {
            String message = "Contract #" + contract.getId() + " send to " + contract.getAuthor().toString() + "!";
            facesContext.addMessage("offerContractForm:offerContractBtn",
                    new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
            log.info(message);
        }
        selectedAuthorId = null;
    }

    public void publishBook() {
        log.info("publishing contract to author");
        if (selectedBookId == null) {
            log.info("bad request!");
            String message = "Please select book.";
            facesContext.addMessage("publishBookForm:publishBookBtn",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
            return;
        }

        Book book;
        try {
            book = publisherService.publishBook(selectedBookId, publisher.getId());
            if (book == null) {
                log.info("bad request!");
                badRequestMessage();
            } else {
                String message = "Book #" + book.getId() + " published!";
                facesContext.addMessage("publishBookForm:publishBookBtn",
                        new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
                log.info(message);
            }
        } catch (ContractException e) {
            log.info(e.getMessage());
            facesContext.addMessage("publishBookForm:publishBookBtn",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
        }
        selectedBookId = null;
    }


    public Long getSelectedAuthorId() {
        return selectedAuthorId;
    }

    public void setSelectedAuthorId(Author selectedAuthor) {
        this.selectedAuthorId = selectedAuthor.getId();
        String message = selectedAuthor.toString() + " selected!";
        log.info(message);
        facesContext.addMessage("offerContractForm:selectAuthorBtn",
                new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }

    public Long getSelectedBookId() {
        return selectedBookId;
    }

    public void setSelectedBookId(Book selectedBook) {
        this.selectedBookId = selectedBook.getId();
        String message = selectedBook.toString() + " selected!";
        log.info(message);
        facesContext.addMessage("publishBookForm:selectBookBtn",
                new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }

    public List<Author> getAuthors() {
        return authorDAO.list();
    }

    public List<Book> getBooks() {
        return bookDAO.list();
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }
}
