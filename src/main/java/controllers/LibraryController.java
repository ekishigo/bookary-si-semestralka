package controllers;

import dao.BookDAO;
import dao.LibraryDAO;
import exceptions.LibraryBookException;
import models.Book;
import models.Library;
import models.LibraryBook;
import org.slf4j.Logger;
import services.LibraryService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by igorekishev on 09.01.2017.
 * Project DIMASIS
 */
@SuppressWarnings("WeakerAccess")
@ManagedBean
@ViewScoped
public class LibraryController {
    @Inject
    private Logger log;
    @Inject
    private LibraryDAO libraryDAO;
    @Inject
    private FacesContext facesContext;
    @Inject
    private BookDAO bookDAO;
    @Inject
    private LibraryService libraryService;

    private Long libraryId;
    private Library library;

    private Long selectedBookId;

    public void init() {

        if (libraryId == null) {
            String message = "Bad request. Please use a link from within the system.";
            facesContext.addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
            return;
        }
        log.info("initializing library contoller for library [id:{}]", libraryId);
        library = libraryDAO.find(libraryId);

        if (library == null) {
            String message = "Bad request. Unknown library.";
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
            return;
        }
        log.info("library [id:{}] was initialized");
    }

    public List<Book> getBooks() {
        return bookDAO.list();
    }

    public Library getLibrary() {
        return library;
    }

    public void setLibrary(Library library) {
        this.library = library;
    }

    public Long getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(Long libraryId) {
        this.libraryId = libraryId;
    }

    public Long getSelectedBookId() {
        return selectedBookId;
    }

    public void setSelectedBookId(Book book) {
        this.selectedBookId = book.getId();
        String message = book.toString() + " selected!";
        log.info(message);
        facesContext.addMessage("addBookForm:selectBookBtn",
                new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }

    public void addBook() {
        log.info("adding book to library");
        if (selectedBookId == null) {
            log.info("bad request!");
            String message = "Please select book.";
            facesContext.addMessage("addBookForm:addBookBtn",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
            return;
        }

        LibraryBook book;
        try {
            book = libraryService.addCopyToLibrary(selectedBookId, library.getId());
            if (book == null) {
                log.info("bad request!");
                badRequestMessage();
            } else {
                String message = "Book #" + book.getId() + " added!";
                facesContext.addMessage("addBookForm:addBookBtn",
                        new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
                log.info(message);
            }
        } catch (LibraryBookException e) {
            log.info(e.getMessage());
            facesContext.addMessage("addBookForm:addBookBtn",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
        }
        selectedBookId = null;
    }

    private void badRequestMessage() {
        String message = "Bad request. Please use a link from within the system.";
        facesContext.addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }
}
