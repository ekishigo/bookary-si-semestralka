package controllers;

import dao.AuthorDAO;
import models.Author;
import models.Contract;
import org.slf4j.Logger;
import services.ContractService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@ManagedBean
@ViewScoped
public class AuthorController {
    @Inject
    private Logger log;
    @Inject
    private AuthorDAO authorDAO;
    @Inject
    private FacesContext facesContext;
    @Inject
    private ContractService contractService;

    private Long authorId;
    private Author author;

    private Long selectedContractId;

    public void init() {

        if (authorId == null) {
            String message = "Bad request. Please use a link from within the system.";
            facesContext.addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
            return;
        }
        log.info("initializing author contoller for author [id:{}]", authorId);
        author = authorDAO.find(authorId);

        if (author == null) {
            String message = "Bad request. Unknown author.";
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
            return;
        }
        log.info("author [id:{}] was initialized");
    }

    public void concludeContract() {
        log.info("concluding contract to author");
        if (selectedContractId == null) {
            log.info("bad request!");
            String message = "Please select contract.";
            facesContext.addMessage("concludeContractForm:concludeContractBtn",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
            return;
        }

        Contract contract;
        contract = contractService.authorConcludeContract(selectedContractId);
        if (contract == null) {
            log.info("bad request!");
            badRequestMessage();
        } else {
            String message = "Contract #" + contract.getId() + " concluded!";
            facesContext.addMessage("concludeContractForm:concludeContractBtn",
                    new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
            log.info(message);
        }
        selectedContractId = null;
    }

    private void badRequestMessage() {
        String message = "Bad request. Please use a link from within the system.";
        facesContext.addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    public Long getSelectedContractId() {
        return selectedContractId;
    }

    public void setSelectedContractId(Contract selectedContract) {
        this.selectedContractId = selectedContract.getId();
        String message = selectedContract.toString() + " selected!";
        log.info(message);
        facesContext.addMessage("concludeContractForm:selectContractBtn",
                new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

}
