package services;

import com.google.common.base.Preconditions;
import dao.BookDAO;
import dao.PublisherDAO;
import models.Book;
import models.Publisher;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Stateless
public class PublisherServiceImpl implements PublisherService {

    @Inject
    private Logger log;
    @Inject
    private BookDAO bookDAO;
    @Inject
    private PublisherDAO publisherDAO;

    @Override
    public Book publishBook(Long idBook, Long idPublisher) {
        log.info("publishing book (id:{}) by publisher (id:{})", idBook, idPublisher);
        Book book;
        Publisher publisher;

        book = bookDAO.find(idBook);
        publisher = publisherDAO.find(idPublisher);
        Preconditions.checkArgument((book != null) && (publisher != null),
                "book[%s] or publisher[%s] not found", book, publisher);

        book.publish(publisher);
        bookDAO.update(book);
        log.info("book [id={}] published by publisher [id:{}]", idBook, idPublisher);

        return book;
    }
}
