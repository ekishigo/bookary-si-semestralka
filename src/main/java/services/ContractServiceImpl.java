package services;

import dao.AuthorDAO;
import dao.ContractDAO;
import dao.PublisherDAO;
import models.Author;
import models.Contract;
import models.Publisher;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Stateless
public class ContractServiceImpl implements ContractService {

    @Inject
    private Logger log;
    @Inject
    private ContractDAO contractDAO;
    @Inject
    private PublisherDAO publisherDAO;
    @Inject
    private AuthorDAO authorDAO;

    @Override
    public Contract publisherOfferContractToAuthor(Long idPublisher, Long idAuthor) {
        log.info("creating contract by publisher (id:{}), and sending to author (id:{})", idPublisher, idAuthor);

        Publisher publisher = publisherDAO.find(idPublisher);
        Author author = authorDAO.find(idAuthor);

        if (publisher == null || author == null) {
            log.warn("publisher or author not found");
            return null;
        }

        Contract contract = publisher.makeContractFor(author);
        contractDAO.save(contract);
        publisherDAO.update(publisher);
        log.info("contract saved (id:{}) and assigned to publisher (id:{})", contract.getId(), publisher.getId());

        publisher.sendContractToAuthor(contract, author);
        authorDAO.update(author);
        log.info("contract (id:{}) assigned to author (id:{})", contract.getId(), author.getId());

        return contract;
    }

    @Override
    public Contract authorConcludeContract(Long idContract) {
        log.info("concluding contract (id:{}) by author", idContract);

        Contract contract = contractDAO.find(idContract);

        if (contract == null) {
            log.warn("contract not found...");
            return null;
        }

        contract.conclude();
        contractDAO.update(contract);
        log.info("contract concluded (id:{})", idContract);

        return contract;
    }
}
