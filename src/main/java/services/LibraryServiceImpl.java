package services;

import com.google.common.base.Preconditions;
import dao.BookDAO;
import dao.LibraryBookDAO;
import dao.LibraryDAO;
import models.Book;
import models.Library;
import models.LibraryBook;
import org.slf4j.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Stateless
public class LibraryServiceImpl implements LibraryService {
    @Inject
    private Logger log;
    @Inject
    private LibraryDAO libraryDAO;
    @Inject
    private LibraryBookDAO libraryBookDAO;
    @Inject
    private BookDAO bookDAO;

    @Override
    public LibraryBook addCopyToLibrary(Long idBook, Long idLibrary) {
        log.info("adding copy of the book (id:{}) to library (id:{})", idBook, idLibrary);
        Book book;
        Library library;

        book = bookDAO.find(idBook);
        library = libraryDAO.find(idLibrary);
        Preconditions.checkArgument((book != null) && (library != null),
                "book[%s] or library[%s] not found", book, library);

        LibraryBook libraryBook = library.addCopy(book);
        libraryBookDAO.save(libraryBook);
        libraryDAO.update(library);
        log.info("copy of the book (id:{}) added to library (id:{})", idBook, idLibrary);

        return libraryBook;
    }
}
