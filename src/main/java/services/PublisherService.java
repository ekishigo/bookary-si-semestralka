package services;

import models.Book;

import javax.validation.constraints.NotNull;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface PublisherService {
    Book publishBook(@NotNull Long idBook,@NotNull Long idPublisher);
}
