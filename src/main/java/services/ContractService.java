package services;

import models.Contract;

import javax.validation.constraints.NotNull;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface ContractService {
    Contract publisherOfferContractToAuthor(@NotNull Long idPublisher,@NotNull Long idAuthor);
    Contract authorConcludeContract(@NotNull Long idContract);
}
