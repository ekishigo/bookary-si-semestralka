package services;

import models.LibraryBook;

import javax.validation.constraints.NotNull;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface LibraryService {
    LibraryBook addCopyToLibrary(@NotNull Long idBook, @NotNull Long idLibrary);
}
