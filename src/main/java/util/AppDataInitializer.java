package util;

import com.google.common.collect.Lists;
import dao.AuthorDAO;
import dao.BookDAO;
import dao.LibraryDAO;
import dao.PublisherDAO;
import models.Author;
import models.Book;
import models.Library;
import models.Publisher;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@Singleton
@Startup
public class AppDataInitializer {
    public static final String[] BOOK_ISBNS = {
            "1",
            "2",
            "3"
    };
    private static final String[] AUTHOR_NAMES = {
            "Fedor",
            "Karel",
            "Isaak"
    };
    private static final String[] AUTHOR_LAST_NAMES = {
            "Dostojevsky",
            "Capek",
            "Azimov"
    };
    private static final String[] PUBLISHER_NAMES = {
            "Academia",
            "Karolinum",
            "Librex"
    };
    private static final String[] PUBLISHER_ADDRESSES = {
            "Lucemburska 33",
            "Jankovcova 124",
            "Delnicka 12"
    };
    private static final String[] BOOK_TITLES = {
            "Idiot",
            "R.U.R.",
            "Foundation"
    };
    private static final String[] LIBRARY_NAME = {
            "Knihovna CVUT",
            "Knihovna Klementium",
            "Knihovna Karlovy Univerzity"
    };
    private static final String[] LIBRARY_ADDRESSES = {
            "Technická 2710/6",
            "library",
            "Senovážné nám. 26"
    };
    @Inject
    private AuthorDAO authorDao;
    @Inject
    private BookDAO bookDao;
    @Inject
    private LibraryDAO libraryDao;
    @Inject
    private PublisherDAO publisherDao;
    @Inject
    private Logger log;


    @PostConstruct
    public void initAppData() {
        log.info("Semestralka Application Startup. Data initialization.");
        initAuthorsWithBooks();
        initLibraries();
        initPublishers();
        log.info("Data initialized.");
    }

    @SuppressWarnings("unused")
    private void initPublishers() {
        for (int i = 0; i < PUBLISHER_NAMES.length; i++) {
            Publisher publisher = Publisher.create(PUBLISHER_NAMES[i]);
            publisher.setContactEmail(PUBLISHER_NAMES[i] + "@mail.com");
            publisher.setContactAddress(PUBLISHER_ADDRESSES[i]);
            publisherDao.save(publisher);
        }
    }

    @SuppressWarnings("unused")
    private void initLibraries() {
        for (int i = 0; i < LIBRARY_NAME.length; i++) {
            Library library = Library.create(LIBRARY_NAME[i], LIBRARY_ADDRESSES[i]);
            libraryDao.save(library);
        }
    }

    @SuppressWarnings("unused")
    private void initAuthorsWithBooks() {
        for (int i = 0; i < AUTHOR_NAMES.length; i++) {
            Author author = Author.create(AUTHOR_NAMES[i], AUTHOR_LAST_NAMES[i]);
            author.setEmail(AUTHOR_LAST_NAMES[i] + "@mail.com");
            authorDao.save(author);
            Book book = Book.create(String.valueOf(i), BOOK_TITLES[i], Lists.newArrayList(author));
            bookDao.save(book);
            author.getWrittenBooks().add(book);
            authorDao.update(author);
        }
    }
}
