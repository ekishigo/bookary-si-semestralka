package dao;

import models.Library;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface LibraryDAO extends DAO<Library> {
}
