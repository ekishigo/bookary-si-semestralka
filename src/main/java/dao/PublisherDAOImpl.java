package dao;

import models.Publisher;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@SuppressWarnings("WeakerAccess")
public class PublisherDAOImpl extends GenericDAO<Publisher> implements PublisherDAO {
}
