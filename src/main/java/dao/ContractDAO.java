package dao;

import models.Contract;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface ContractDAO extends DAO<Contract>  {
}
