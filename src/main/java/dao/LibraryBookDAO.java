package dao;

import models.LibraryBook;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface LibraryBookDAO extends DAO<LibraryBook> {
}
