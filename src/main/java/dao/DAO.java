package dao;

import java.util.List;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
interface DAO<T> {

    List<T> list();

    T save(T entity);

    void delete(Object id);

    T find(Object id);

    T update(T entity);
}