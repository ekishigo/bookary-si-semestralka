package dao;

import models.Publisher;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface PublisherDAO extends DAO<Publisher> {
}
