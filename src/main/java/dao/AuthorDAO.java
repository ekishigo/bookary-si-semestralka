package dao;

import models.Author;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
public interface AuthorDAO extends DAO<Author>{
}
