package dao;

import models.Library;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@SuppressWarnings("WeakerAccess")
public class LibraryDAOImpl extends GenericDAO<Library> implements LibraryDAO {
}
