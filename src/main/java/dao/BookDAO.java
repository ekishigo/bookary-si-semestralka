package dao;

import models.Book;

/**
 * semestralkaigor
 * Created by igor on 21.11.16.
 */
public interface BookDAO extends DAO<Book>{
}
