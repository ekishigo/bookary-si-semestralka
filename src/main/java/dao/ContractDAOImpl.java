package dao;

import models.Contract;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@SuppressWarnings("WeakerAccess")
public class ContractDAOImpl extends GenericDAO<Contract> implements ContractDAO {
}
