package dao;

import models.LibraryBook;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@SuppressWarnings("WeakerAccess")
public class LibraryBookDAOImpl extends GenericDAO<LibraryBook>  implements LibraryBookDAO {
}
