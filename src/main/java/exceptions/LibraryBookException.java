package exceptions;

import javax.ejb.ApplicationException;

/**
 * Created by igorekishev on 08.01.2017.
 * Project semestralka
 */
@ApplicationException
public class LibraryBookException extends RuntimeException {
    public LibraryBookException(String message) {
        super(message);
    }
}
