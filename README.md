Bookary semestral project
===

Deployment
---

first check data source, should be same as declared in persistence.xml!

- Run WildFly server
- Navigate to this project's root folder and open a command line
- Run the following command to deploy the application
```mvn clean package wildfly:deploy```
- Open your browser and navigate to `localhost:8080/semestralka`

Running tests
---

```
mvn clean test -Parq-wildfly-remote
```
